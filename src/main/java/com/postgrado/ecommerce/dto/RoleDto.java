package com.postgrado.ecommerce.dto;

public class RoleDto {
  private String name;
  private String description;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return name;
  }
  public void setDescription() {
    this.description = description;
  }
}
