package com.postgrado.ecommerce.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.postgrado.ecommerce.entity.OrderState;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class OrderDto {
  private String comment;
  List<OrderItemDto> items;

  @JsonProperty(access = Access.READ_ONLY)
  private Double totalPrice;

  @JsonProperty(access = Access.READ_ONLY)
  private OrderState state;
}
