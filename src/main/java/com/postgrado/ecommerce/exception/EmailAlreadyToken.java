package com.postgrado.ecommerce.exception;

public class EmailAlreadyToken extends RuntimeException {
  private static final String ERROR_MESSAGE = "Email %s is already taken";

  public EmailAlreadyToken(String email) {
    super(String.format(ERROR_MESSAGE, email));
  }

}
