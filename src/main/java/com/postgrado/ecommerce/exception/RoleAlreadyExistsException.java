package com.postgrado.ecommerce.exception;

public class RoleAlreadyExistsException extends RuntimeException {
  private String roleName;

  public RoleAlreadyExistsException(String roleName) {
    this.roleName = roleName;
  }

  public String getRoleName() {
    return roleName;
  }
}

