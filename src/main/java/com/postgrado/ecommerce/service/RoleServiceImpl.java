package com.postgrado.ecommerce.service;

import com.postgrado.ecommerce.dto.RoleDto;
import com.postgrado.ecommerce.entity.Role;
import com.postgrado.ecommerce.exception.RoleAlreadyExistsException;
import com.postgrado.ecommerce.repository.RoleRepository;
import com.postgrado.ecommerce.exception.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class RoleServiceImpl implements RoleService{
  private RoleRepository roleRepository;

  @Override
  public Role getByName(String name) {
    return roleRepository.findByName(name)
        .orElseThrow(() -> new EntityNotFoundException("Role not found"));
  }

  @Override
  public List<Role> getAll() {

    return  roleRepository.findAll();
  }

  @Override
  public Role create(RoleDto roleDto) {
    String roleName = roleDto.getName();

    if (roleRepository.existsByName(roleName)) {
      throw new RoleAlreadyExistsException(roleName);
    }

    Role role = new Role();
    role.setName(roleName);
    role.setDescription(roleDto.getDescription());

    return roleRepository.save(role);
  }


  @Override
  public boolean existsByName(String name) {
    return roleRepository.existsByName(name);
  }

}
