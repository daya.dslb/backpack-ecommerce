package com.postgrado.ecommerce.service;

import com.postgrado.ecommerce.entity.ConfirmationToken;
import com.postgrado.ecommerce.repository.ConfirmationTokenRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class ConfirmationTokenImpl implements ConfirmationTokenService{
  private ConfirmationTokenRepository repository;

  @Override
  public ConfirmationToken create(ConfirmationToken confirmationToken) {
    return repository.save(confirmationToken);
  }
}
