package com.postgrado.ecommerce.service;

import com.postgrado.ecommerce.entity.Category;
import com.postgrado.ecommerce.exception.EntityNotFoundException;
import com.postgrado.ecommerce.repository.CategoryRepository;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CategoryServiceImplement implements CategoryService{

  private CategoryRepository categoryRepository;

  @Override
  public Category getById(UUID id) {
    Category category = categoryRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException("Category", id));
    return category;
  }

  @Override
  public List<Category> getAll() {
    return categoryRepository.findAll();
  }
}
