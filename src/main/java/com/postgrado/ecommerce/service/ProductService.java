package com.postgrado.ecommerce.service;

import com.postgrado.ecommerce.dto.PageDto;
import com.postgrado.ecommerce.entity.Product;
import com.postgrado.ecommerce.dto.ProductoDto;
import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductService {
  Product create(ProductoDto dto);

  Product getById(UUID id);

  Page<Product> getProducts(Pageable pageable);

  PageDto<Product> getFilteredProducts(Double minPrice, Double maxPrice, Pageable pageable);

  Product updateById(UUID id, ProductoDto dto);


  Page<Product> getProductsByCategoryId(UUID categoryId, Pageable pageable);
}
