package com.postgrado.ecommerce.service;

import com.postgrado.ecommerce.dto.PageDto;
import com.postgrado.ecommerce.entity.Category;
import com.postgrado.ecommerce.entity.Product;
import com.postgrado.ecommerce.repository.ProductRepository;
import com.postgrado.ecommerce.dto.ProductoDto;
import com.postgrado.ecommerce.exception.EntityNotFoundException;
import java.util.UUID;
import lombok.AllArgsConstructor;
import com.postgrado.ecommerce.mapper.ProductMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class ProductServiceImpl implements ProductService {

  private ProductRepository productRepository;
  private CategoryService categoryService;

  private ProductMapper productMapper;

  @Override
  public Product create(ProductoDto dto) {
    Category category = categoryService.getById(dto.getCategoryId());
    Product product = productMapper.fromDto(dto);
    product.setCategory(category);
    return productRepository.save(product);
  }

  @Override
  public Product getById(UUID id) {
    return productRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException("Product", id));
  }

  @Override
  public Page<Product> getProducts(Pageable pageable) {
    return productRepository.findAll(pageable);
  }

  @Override
  public PageDto<Product> getFilteredProducts(Double minPrice, Double maxPrice, Pageable pageable) {
    Page<Product> page = productRepository.findByPriceBetween(minPrice, maxPrice, pageable);
    return productMapper.fromEntity(page);
  }

  @Override
  public Product updateById(UUID id, ProductoDto dto) {
    Product product = productRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException("Product", id));

    product.setName(dto.getName());
    product.setDescription(dto.getDescription());

    return productRepository.save(product);
  }

  @Override
  public Page<Product> getProductsByCategoryId(UUID categoryId, Pageable pageable) {
    return productRepository.findByCategoryId(categoryId, pageable);
  }


}
