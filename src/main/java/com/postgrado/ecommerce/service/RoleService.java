package com.postgrado.ecommerce.service;

import com.postgrado.ecommerce.dto.RoleDto;
import com.postgrado.ecommerce.entity.Role;
import java.util.List;

public interface RoleService {
  Role getByName(String name);
  Role create(RoleDto roleDto);
  boolean existsByName(String name);
  List<Role> getAll();
}
