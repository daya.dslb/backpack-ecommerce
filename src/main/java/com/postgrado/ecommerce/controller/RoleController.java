package com.postgrado.ecommerce.controller;

import com.postgrado.ecommerce.dto.ErrorResponse;
import com.postgrado.ecommerce.dto.RoleDto;
import com.postgrado.ecommerce.entity.Role;
import com.postgrado.ecommerce.exception.RoleAlreadyExistsException;
import com.postgrado.ecommerce.service.RoleService;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/roles")
public class RoleController {
  private RoleService roleService;

  @GetMapping("/name/{name}")
  public ResponseEntity<Role> getByName(@PathVariable String name) {
    Role roleFound = roleService.getByName(name);
    return ResponseEntity.status(HttpStatus.OK).body(roleFound);
  }

  @GetMapping
  public ResponseEntity<List<Role>> getAll() {
    List<Role> roles = roleService.getAll();
    return ResponseEntity.ok(roles);
  }

  @PostMapping
  public ResponseEntity<?> createRole(@RequestBody RoleDto roleDto) {
    String roleName = roleDto.getName();

    if (roleService.existsByName(roleName)) {
      ErrorResponse errorResponse = new ErrorResponse(409, "Role Name Already Exists", "Role with name " + roleName + " already exists.");
      return ResponseEntity.status(HttpStatus.CONFLICT).body(errorResponse);
    }

    Role createdRole = roleService.create(roleDto);
    return ResponseEntity.status(HttpStatus.CREATED).body(createdRole);
  }
  @ExceptionHandler(RoleAlreadyExistsException.class)
  public ResponseEntity<ErrorResponse> handleRoleAlreadyExistsException(RoleAlreadyExistsException ex) {
    ErrorResponse errorResponse = new ErrorResponse(409, "Role Name Already Exists", "Role with name " + ex.getRoleName() + " already exists.");
    return ResponseEntity.status(HttpStatus.CONFLICT).body(errorResponse);
  }

}
